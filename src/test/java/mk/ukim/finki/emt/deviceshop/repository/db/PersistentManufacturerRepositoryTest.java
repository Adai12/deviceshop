package mk.ukim.finki.emt.deviceshop.repository.db;

import mk.ukim.finki.emt.deviceshop.models.Device;
import mk.ukim.finki.emt.deviceshop.models.Manufacturer;
import mk.ukim.finki.emt.deviceshop.repository.jpa.JpaDeviceRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@DataJpaTest
public class PersistentManufacturerRepositoryTest {

    @Autowired
    private PersistentManufacturerRepository repo;

    @Autowired
    private JpaDeviceRepository deviceRepository;

    @Before
    public void init() {

        Manufacturer m = new Manufacturer();
        m.setName("SAMSUNG");
        repo.save(m);

        Device d1 = new Device();
        d1.setName("Galaxy");
        d1.setPrice(10d);
        d1.setManufacturer(m);
        deviceRepository.save(d1);

        Device d2 = new Device();
        d2.setName("Y6");
        d2.setPrice(20d);
        d2.setManufacturer(m);
        deviceRepository.save(d2);

        List<Device> deviceList = new ArrayList<>();
        deviceList.add(d1);
        deviceList.add(d2);
//        m.setDeviceList(deviceList);
        repo.save(m);


    }

    @Test
    public void findAll() {
        List<Manufacturer> deviceList = repo.findAll();
//        Assert.assertEquals(2,deviceList.get(0).getDeviceList().size());
    }

    @Test
    public void getById() {
    }
}