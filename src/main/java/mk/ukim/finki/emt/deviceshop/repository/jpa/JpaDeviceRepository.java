package mk.ukim.finki.emt.deviceshop.repository.jpa;

import mk.ukim.finki.emt.deviceshop.dto.DeviceReduced;
import mk.ukim.finki.emt.deviceshop.models.Category;
import mk.ukim.finki.emt.deviceshop.models.Device;
import mk.ukim.finki.emt.deviceshop.models.projections.DeviceProjection;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface JpaDeviceRepository extends JpaRepository<Device, Long> {

    Device save(Device d);

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH,
            attributePaths = {"manufacturer"})
    List<Device> findAll();

    List<Device> findByCategoryId(Long id);

    List<Device> findByCategoryIdAndManufacturerId(Long categoryId, Long manufacturerId);

    long count();

    long countByCategoryId(Long categoryId);

    List<Device> findByCategoryIdOrManufacturerId(Long categoryId, Long manufacturerId);

    List<Device> findByManufacturerNameLike(String mName);

    List<DeviceProjection> findBy();

    List<Device> findByManufacturerId(Long manId);
}
