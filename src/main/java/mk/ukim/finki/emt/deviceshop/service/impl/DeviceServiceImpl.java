package mk.ukim.finki.emt.deviceshop.service.impl;

import mk.ukim.finki.emt.deviceshop.exceptions.DeviceNotFoundException;
import mk.ukim.finki.emt.deviceshop.exceptions.ManufacturerNotFoundException;
import mk.ukim.finki.emt.deviceshop.models.Device;
import mk.ukim.finki.emt.deviceshop.models.Manufacturer;
import mk.ukim.finki.emt.deviceshop.repository.DeviceRepository;
import mk.ukim.finki.emt.deviceshop.service.DeviceService;
import mk.ukim.finki.emt.deviceshop.service.ManufacturerService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Profile("in-memory")
public class DeviceServiceImpl implements DeviceService {

    private DeviceRepository repo;

    private ManufacturerService manufacturerService;

    public DeviceServiceImpl(DeviceRepository repo, ManufacturerService manufacturerService) {
        this.repo = repo;
        this.manufacturerService = manufacturerService;
    }

    @Override
    public Device addNewDevice(Device device, Long manufacturerId, String username) throws ManufacturerNotFoundException {
        Optional<Manufacturer> man = manufacturerService.getAll().stream().filter(v -> v.getId().equals(manufacturerId)).findAny();
        if (!man.isPresent()) {
            throw new ManufacturerNotFoundException();
        }
        device.setManufacturer(man.get());
        return repo.save(device);
    }

    @Override
    public Device addNewDevice(String name, Double price, Long manufacturerId) throws ManufacturerNotFoundException {
        Device d = new Device();
        d.setName(name);
        d.setPrice(price);
        Optional<Manufacturer> man = manufacturerService.getAll().stream().filter(v -> v.getId().equals(manufacturerId)).findAny();
        if (!man.isPresent()) {
            throw new ManufacturerNotFoundException();
        }
        d.setManufacturer(man.get());
        repo.save(d);
        return d;
    }

    @Override
    public Device addNewDevice(String name, Double price, Long manufacturerId, byte[] photo) throws ManufacturerNotFoundException {
        Device d = new Device();
        d.setName(name);
        d.setPrice(price);
        Optional<Manufacturer> man = manufacturerService.getAll().stream().filter(v -> v.getId().equals(manufacturerId)).findAny();
        if (!man.isPresent()) {
            throw new ManufacturerNotFoundException();
        }
        d.setManufacturer(man.get());
        d.setPhoto(photo);
        repo.save(d);
        return d;
    }


    @Override
    public Device addNewDevice(Device device, String username) {
        return repo.save(device);
    }

    @Override
    public List<Device> getAllDevices() {
        return repo.findAllDevices();
    }

    @Override
    public Device update(Device device) throws DeviceNotFoundException {
        return null;
    }

    @Override
    public void delete(Long deviceId) {
        repo.delete(deviceId);
    }

    @Override
    public Device getById(Long deviceId) {
        Optional<Device> device = repo.findById(deviceId);
        if (!device.isPresent()) {
            throw new DeviceNotFoundException();
        }
        return device.get();
    }

    @Override
    public List<Device> getByManId(Long manId) {
        return null;
    }
}
