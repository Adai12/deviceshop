package mk.ukim.finki.emt.deviceshop.repository;

import mk.ukim.finki.emt.deviceshop.models.Category;
import org.springframework.data.repository.Repository;

import java.util.Optional;

public interface PersistentCategoryRepository extends Repository<Category,Long> {
    void save(Category c);
    Optional<Category> findById(Long id);
}
