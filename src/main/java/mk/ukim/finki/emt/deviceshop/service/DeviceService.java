package mk.ukim.finki.emt.deviceshop.service;

import mk.ukim.finki.emt.deviceshop.exceptions.DeviceNotFoundException;
import mk.ukim.finki.emt.deviceshop.exceptions.ManufacturerNotFoundException;
import mk.ukim.finki.emt.deviceshop.models.Device;

import java.util.List;

public interface DeviceService {
    Device addNewDevice(Device device, Long manufacturerId,  String username);
    Device addNewDevice(String name, Double price, Long manufacturerId) throws ManufacturerNotFoundException;
    Device addNewDevice(String name, Double price, Long manufacturerId, byte[] photo) throws ManufacturerNotFoundException;
    Device addNewDevice(Device device, String username);
    List<Device> getAllDevices();
    Device update(Device device) throws DeviceNotFoundException;
    void delete(Long deviceId);

    Device getById(Long deviceId);

    List<Device> getByManId(Long manId);
}
