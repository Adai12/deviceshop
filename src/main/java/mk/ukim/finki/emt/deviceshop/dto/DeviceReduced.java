package mk.ukim.finki.emt.deviceshop.dto;

import mk.ukim.finki.emt.deviceshop.models.Device;

public class DeviceReduced {

    private Long id;
    private String name;

    public DeviceReduced(Device d) {
        this.id= d.getId();
        this.name= d.getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
