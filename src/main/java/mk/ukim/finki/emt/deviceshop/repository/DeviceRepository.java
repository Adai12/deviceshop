package mk.ukim.finki.emt.deviceshop.repository;

import mk.ukim.finki.emt.deviceshop.exceptions.ManufacturerNotFoundException;
import mk.ukim.finki.emt.deviceshop.models.Device;
import mk.ukim.finki.emt.deviceshop.models.Manufacturer;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class DeviceRepository {

    private Long counter;
    private List<Device> deviceList = null;

    @PostConstruct
    public void init() {
        counter = 1L;

        deviceList = new ArrayList<>();
        Device d1 = new Device();
        d1.setId(getNextId());
        d1.setName("Galaxy S5");
        d1.setPrice(2000.0);
        deviceList.add(d1);

        Device d2 = new Device();
        d2.setId(getNextId());
        d2.setName("Mi");
        d2.setPrice(2000.0);
        deviceList.add(d2);
    }

    public List<Device> findAllDevices() {
        return deviceList;
    }

    public Device save(Device device){
        device.setId(getNextId());
        deviceList.add(device);
        return device;
    }

    public void delete(Long deviceId) {
        deviceList.removeIf(v-> {
            return v.getId().equals(deviceId);
        });
    }

    private Long getNextId() {
        return counter++;
    }

    public Optional<Device> findById(Long deviceId) {
        return deviceList.stream().filter(v-> v.getId().equals(deviceId)).findAny();
    }
}
