package mk.ukim.finki.emt.deviceshop.web.rest;

import mk.ukim.finki.emt.deviceshop.exceptions.ManufacturerNotFoundException;
import mk.ukim.finki.emt.deviceshop.models.Device;
import mk.ukim.finki.emt.deviceshop.models.Manufacturer;
import mk.ukim.finki.emt.deviceshop.service.DeviceService;
import mk.ukim.finki.emt.deviceshop.service.ManufacturerService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/rest/device")
public class DeviceRestfulResource {

    private DeviceService deviceService;
    private ManufacturerService manufacturerService;

    public DeviceRestfulResource(DeviceService deviceService, ManufacturerService manufacturerService) {
        this.deviceService = deviceService;
        this.manufacturerService = manufacturerService;
    }

    @GetMapping
    public List<Device> getAllDevices() {
        return deviceService.getAllDevices();
    }

    @PostMapping
    public Device createNewDevice(@RequestBody Device device, HttpSession session) {
        return deviceService.addNewDevice(device, (String)session.getAttribute("firstName"));
    }


    @GetMapping("/manufacturer/{manId}")
    public List<Device> getAllByManufacturer(@PathVariable("manId") Long manId) {
        List<Device> devices = deviceService.getAllDevices();
        return devices;
    }

    @GetMapping("/manufacturer")
    public List<Device> getAllByMan(@RequestParam("mId") Long id) {
        Manufacturer m = manufacturerService.getOne(id).orElseThrow(()->new ManufacturerNotFoundException());
        return m.getDevices();

    }

}
